mkdir dotnet

wget https://download.visualstudio.microsoft.com/download/pr/349f13f0-400e-476c-ba10-fe284b35b932/44a5863469051c5cf103129f1423ddb8/dotnet-sdk-3.1.102-linux-arm.tar.gz
wget https://download.visualstudio.microsoft.com/download/pr/8ccacf09-e5eb-481b-a407-2398b08ac6ac/1cef921566cb9d1ca8c742c9c26a521c/aspnetcore-runtime-3.1.2-linux-arm.tar.gz

tar zxf dotnet-sdk-3.1.102-linux-arm.tar.gz -C dotnet
tar zxf aspnetcore-runtime-3.1.2-linux-arm.tar.gz -C dotnet

rm dotnet-sdk-3.1.102-linux-arm.tar.gz
rm aspnetcore-runtime-3.1.2-linux-arm.tar.gz
