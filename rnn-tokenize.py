import time, codecs, os, collections
import re

use2me = True
prefix = 'trash'

textFile = '{}Messages.txt'.format(prefix)
wordFile = '{}Words.txt'.format(prefix)
tokenFile = '{}Tokens.txt'.format(prefix)


def makeWordFile(wordFile, textFile):
    startTime = time.time()
    wordCount = 0
    with codecs.open(textFile, 'r', encoding='UTF-8') as tf:
        with codecs.open(wordFile, 'w', encoding='UTF-8') as wf:
            for line in tf:
                if line == '\n':
                    continue
                line = line[:-1]
                m = re.search('https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)', line)
                # line.replace('`',' ')
                line = ' '.join(line.split('`'))
                words = line.split(' ')
                for word in words:
                    wordCount += 1
                    wf.write(word+'\n')
    print('Read {:,} words ({:.2f}s)'.format(wordCount, time.time()-startTime))

makeWordFile(wordFile, textFile)

wf = codecs.open(wordFile, 'r', encoding='UTF-8')

cnt = collections.Counter()

print('Tallying strings')
for i, line in enumerate(wf):
    line = line[:-1]
    cnt[line] += 1
print('Fount {:,} unique strings'.format(len(cnt)))

mostCommon = cnt.most_common()
with codecs.open(tokenFile, 'w', encoding='UTF-8') as tf:
    for i in range(len(mostCommon)):
        line = mostCommon[i][0] + ' ' + str(mostCommon[i][1]) + '\n'
        tf.write(line)

print('Done!')