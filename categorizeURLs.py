import os, codecs, string, time, collections
import re

os.chdir(os.path.dirname(os.path.realpath(__file__)))

prefix = 'trash'
linkFile = prefix+'Links.txt'

def getDomain(link):
    slash = link.find('/')
    slash2 = link.find('/', slash+2)
    domain = link[slash+2:slash2]

    return domain

URLcnt = collections.Counter()
with codecs.open(linkFile, 'r', encoding='UTF-8') as f:
    for line in f:
        line = line[:-1]
        domain = getDomain(line)
        # print(domain)
        URLcnt[domain] += 1


print('Done!')