import os, codecs, string, time

import re
p = re.compile('https?://[a-zA-Z0-9_&?./=-]{0,256}')

tokenizeLinks = False

def readMessages(filename):
    linkData = []
    f = codecs.open(filename, 'r', encoding='UTF-8')
    headerLines = []
    messageData = []
    metadata = {}
    readHeader = False
    for i, line in enumerate(f):
        while line != '' and line[-1] in ['\r','\n']:
            line = line[:-1]
        links = p.findall(line)
        for link in links:
            line = line.replace(link, '')
        line.replace('`','\'')
        if len(line) == 0:
            continue
        if not readHeader:
            if i > 0 and line == '='*62:
                readHeader = True
                serverID = filename[filename.rfind('[')+1:filename.rfind(']')]
                serverName = headerLines[0][7:]
                channelName = headerLines[1][9:]
                channelTopic = headerLines[2][7:]
                if len(headerLines) > 7:
                    channelTopic += '\n'
                    channelTopic += '\n'.join([headerLines[i] for i in range(4, len(headerLines)-3)])
                channelMessages = int(headerLines[-2][10:].replace(',',''))
                channelRange = headerLines[-1][7:]
                metadata = {
                'serverName' : serverName,
                'serverID' : serverID,
                'channelName' : channelName,
                'channelTopic' : channelTopic,
                'channelMessages' : channelMessages,
                'channelRange' : channelRange}
                print('{}, #{} ({:,} messages)'.format(metadata['serverName'], metadata['channelName'], metadata['channelMessages']))
    
            elif i > 0:
                headerLines.append(line)
        else:
            # Main read function
            if isHeader(line):
                messageTime = line[1:19]
                messageUsername = line[21:]
                newMessage = {
                    'time' : messageTime,
                    'username' : messageUsername,
                    'content' : ''}
                if len(messageData) > 0 and messageData[-1]['content'] == '':
                    messageData[-1] = newMessage
                else:
                   messageData.append(newMessage)
            else:
                if messageData[-1]['content'] != '':
                    messageData[-1]['content'] += '\n'
                messageData[-1]['content'] += line
    if tokenizeLinks:
        print('Read {:,} messages, {:,} links'.format(len(messageData), len(linkData)))
    else:
        print('Read {:,} messages'.format(len(messageData)))
    return metadata, messageData, linkData

def combineSameMessages(messages):
    combinedMessages = []
    for i, message in enumerate(messages):
        if combinedMessages == [] or message['username'] != combinedMessages[-1]['username']:
            newMessage = message.copy()
            # newMessage['content'] += '
            combinedMessages.append(newMessage)
        else:
            combinedMessages[-1]['content'] += '\n' + message['content']
    return combinedMessages

def isHeader(line):
    if len(line) < 21:
        return False
    if line[-1] == '\n':
        line = line[:-1]
    if not (line[0] == '[' and line[19] == ']'):
        return False
    if not (line.find('AM') == 17 or line.find('PM') == 17):
        return False
    if not (line[10] == ' ' and line[16] == ' ', line[20] == ' '):
        return False
    if not (line[-5] == '#' and all([c in string.digits for c in line[-4:]])):
        return False
    return True


os.chdir(os.path.dirname(os.path.realpath(__file__)))

sourceDir = 'downloadsTxt'
files = [os.path.join(sourceDir, file) for file in os.listdir(sourceDir)]
outFileMessages = os.path.join('parsed', sourceDir+'MessagesNoLinks.txt')
outFileLinks = os.path.join('parsed', sourceDir+'Links.txt')

if tokenizeLinks:
    messageOut = codecs.open(outFileLinks, 'w', encoding='UTF-8')
with codecs.open(outFileMessages, 'w', encoding='UTF-8') as outFile: 
    for fileNum, filename in enumerate(files):
        startTime = time.time()
        print('({}/{})'.format(fileNum+1, len(files)))
        serverInfo, messageData, linkData = readMessages(filename)
        if serverInfo['channelMessages'] == 0:
            print('No messages in server')
            continue
        oldLen = len(messageData)
        combine = False
        if combine:
            messageData = combineSameMessages(messageData)
            print('Combined into {:,} messages, reducing by {:.0f}%'.format(len(messageData), 100*(1-(len(messageData)/oldLen))))
        # Writing messages    
        for i, message in enumerate(messageData):
            newText = message['content']+'\n' # ` = message end character 
            outFile.write(newText)
        print('Wrote messages')
        if tokenizeLinks:
            # Writing Links
            for i, link in enumerate(linkData):
                messageOut.write(link+'\n')
            print('Wrote links')
        runtime = time.time()-startTime
        print('Total {:.2f}s\n'.format(runtime))
if tokenizeLinks:
    messageOut.close()

print("Done!")