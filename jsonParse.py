import os, codecs, sys, time, string, json
import re, bz2

import utils
from os.path import join
# import progressbar

p = re.compile('https?://[a-zA-Z0-9_&?./=-]{0,256}')

tokenizeLinks = False

def jsonToMessagesText(data):
    allLines = []
    # for message in progressbar.progressbar(data['messages']):
    for message in data['messages']:
        message = message['content']
        links = p.findall(message)
        for link in links:
            message = message.replace(link, '')
        while '\n\n' in message:
            message = message.replace('\n\n','\n')
        while len(message) > 0 and message[-1] == '\n':
            message = message[:-1]
        message = message.replace('\r','').replace('`','\'').replace('\n','`')
        if len(message) > 0:
            allLines.append(message)
    return allLines

# def unzipBZ2(filename, zipDir, unzipDir, overwrite=False):
#     zipFile = bz2.open(join(zipDir, filename), 'rb')
#     unzipFileName = join(unzipDir, filename)[:-4]
#     if os.path.exists(unzipFileName) and not overwrite:
#         return None
#     unzipFile = open(unzipFileName, 'wb')
#     unzipFile.write(zipFile.read())
#     zipFile.close()
#     unzipFile.close()
#     return unzipFileName

os.chdir(os.path.dirname(os.path.realpath(__file__)))

nameLog = 'logs/names.json'

sourceDir = 'downloads'
outFileMessages = join('parsed', sourceDir+'MessagesNoLinks.txt')
outFileLinks = join('parsed', sourceDir+'Links.txt')

zipFiles = [file for file in os.listdir('downloadsCompressed')]


with codecs.open(outFileMessages, 'w', encoding='UTF-8'):
    None

idNames = {'g' : {}, 'c' : {}}
if os.path.exists(nameLog) and os.path.getsize(nameLog) > 0:
    with codecs.open(nameLog, 'r', encoding='UTF-8') as f:
        idNames = json.load(f)

files = [join(sourceDir, file) for file in os.listdir(sourceDir)]

allMessages = []
for i, channelPath in enumerate(zipFiles):
    channel = channelPath[channelPath.rfind('[')+1:channelPath.rfind(']')]
    print('{}/{}: {}'.format(i+1, len(zipFiles), channelPath[:-9]))        
    zipPath, unzipPath = utils.unzipChannel(channel, 'downloads', 'downloadsCompressed')
    with codecs.open(outFileMessages, 'a', encoding='UTF-8') as out:    
        with codecs.open(unzipPath, 'r', encoding='UTF-8') as f:        
            data = json.load(f)
            lines = jsonToMessagesText(data)
            print('Read {:,} messages'.format(len(lines)))
            # allMessages.extend(lines)
            out.write('\n'.join(lines)+'\n')
            print()
    
    os.remove(unzipPath)
        

print('Done!')