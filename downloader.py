import os, codecs, subprocess, time, datetime, csv, shutil, random, psutil, platform
from os.path import join
from os import path
import multiprocessing as mp
from multiprocessing import Pool, Process, Queue
from subprocess import Popen, PIPE

import ntpath
import json
from zipfile import bz2
import utils

os.chdir(os.path.dirname(os.path.realpath(__file__)))
homeDir =  os.path.dirname(os.path.realpath(__file__))
nameLog = 'logs/names.json'

# Memory usage
# process = psutil.Process(os.getpid())
# print(process.memory_info().rss)  # in bytes 
# print()

verbosity = 2
# 1: only process completion
# 2: process completion & start
# 3 Commands too
windows = platform.system() == 'Windows'

exePath = 'dotnet '+os.path.join(homeDir, 'DiscordChatExporter.CLI/DiscordChatExporter.Cli.dll')
paths = {'guild' : 'logs/completedGuilds.txt', 'channelUnavailable' : 'logs/unavailableChannels.txt'}
if windows:
    paths = {'guild' : 'logs\\completedGuilds.txt', 'channelUnavailable' : 'logs/unavailableChannels.txt'}
    exePath = 'dotnet '+os.path.join(homeDir, 'DiscordChatExporter.CLI\\DiscordChatExporter.Cli.dll')
    # exePath = os.path.join(homeDir, 'DiscordChatExporter.CLI\\DiscordChatExporter.Cli.exe')



token = ''
with open('token.txt', 'r') as f:
    token = f.readline().replace('\n', '').replace('\r', '')

downloadPath = join(homeDir, 'downloads')
zipDir = join(homeDir,'downloadsCompressed')

FNULL = open(os.devnull, 'w')

# Tries to match names
idNames = {'g' : {}, 'c' : {}}
if os.path.exists(nameLog) and os.path.getsize(nameLog) > 0:
    with codecs.open(nameLog, 'r', encoding='UTF-8') as f:
        idNames = json.load(f)

def downloadChannelJson(token, channel, directory, exePath, idNames=None):
    # localChannels = getChannelsLocal('downloadsTxt')
    # failedChannels = loadIDFile(paths['channelUnavailable'])
    # if (channel in localChannels+failedChannels) and not force:
    #     return 0 # The channel already exists
    name = ''
    if channel in idNames:
        name = idNames[channel]
    command = ' '.join([exePath, 'export', '-t', token, '-o', directory, '-f Json -c', channel])
    # if windows:
    #         command =  ' '.join([directory[:directory.find(':')]+': &&', exePath, 'export', '-t', token, '-o', directory, '-f Json -c', channel,])
    if verbosity >= 3:
        print(command)
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    # Out of memory: System.Threading.Tasks.TaskCanceledException
    # Not allowed: DiscordChatExporter.Core.Services.Exceptions.HttpErrorStatusCodeException
    i = 0
    success = False
    outputLines = [line.decode('UTF-8', errors='replace').replace('\r','').replace('\n','') for line in process.stdout.readlines()]
    outText = '\n'.join(outputLines)
    if 'FORBIDDEN'.lower() in outText.lower():
        print('Download failed: No access')
        utils.logAppend(paths['channelUnavailable'], channel)
    elif 'contains no messages' in outText:
        print('Error: Empty channel')
        utils.logAppend(paths['channelUnavailable'], channel)
    elif 'System.Threading.Tasks.TaskCanceledException' in outText:
        print('Error: Ran out of memory')
    # elif 'Failed' in outputLines[0]:
    #     print('Error: Unknown error occurred')
    elif 'System.IO.IOException' in outText:
        print('Error: Download already in progress')
    else:
        # Success
        print('Download Successful')
        success = True
    # if verbosity >= 2:
    #     print(outputLines)
    if success:
        compressChannel(channel, directory, zipDir, True)
        return 1
    return 0

def compressChannel(channel, unzipDir, zipDir, deleteOriginal=False):
    files = os.listdir(unzipDir)
    toCompress = ''
    for filename in files:
        # print(filename)
        if channel in filename:
            if filename[-4:] == '.bz2':
                os.remove(join(unzipDir, filename))
            elif filename[-5:] == '.json':
                toCompress = filename
                break
    if toCompress == '':
        return
    # print('Compressing {}'.format(toCompress))
    zipName = join(zipDir, toCompress+'.bz2')
    toCompressPath = join(unzipDir, toCompress)
    inFile = open(toCompressPath, 'rb')
    compressLevel = 3
    outFile = bz2.BZ2File(zipName, 'wb', compresslevel=compressLevel)
    outFile.write(inFile.read())
    outFile.close()
    inFile.close()
    sizes = [os.path.getsize(toCompressPath), os.path.getsize(zipName)]
    print('Reduced {} -> {} ({}%)'.format(utils.getSizeHuman(sizes[0]), utils.getSizeHuman(sizes[1]), int(100*(sizes[1]/sizes[0]))))
    if deleteOriginal:
        os.remove(join(unzipDir, toCompress))

# 2meirl server id: 177192169516302336
# #announcements: 487288519123795968
# #general: 487284236760383498
# 487290065723064320 | modmeetings

forceRetryChannels = False
forceRetryGuilds = False
forceRedownload = False
if __name__ == '__main__':
    utils.ensureExists(downloadPath)
    utils.ensureExists(zipDir)
    utils.ensureExists('logs/names.json')
    for filename in paths.values():
        utils.ensureExists(filename)

    print("Getting list of guilds...")
    guilds, idNames = utils.getGuilds(token, exePath,  idNames)
    # guilds = ['177192169516302336', '603424255035834368', '546515996618391562', '655878279366180914', '505901725111287849', '526393239649910791', '512401049483739167', '577675601737940999', '654459331189800980', '511002223447375872', '493164246486614023', '510703011631333379', '612351838112448525', '302244550313902085', '647615327831719946']

    finishedGuilds = utils.loadIDFile(paths['guild'])
    # finishedChannels = getChannelsLocal(zipDir)
    # failedChannels = loadIDFile(paths['channelUnavailable'])

    
    validGuilds = []
    if forceRetryGuilds:
        print("Force-retrying all {} guilds".format(len(guilds)))
    else:
        for guild in guilds:
            if guild not in finishedGuilds:
                validGuilds.append(guild)
        print("Found {}/{} valid guilds".format(len(validGuilds), len(guilds)))

    random.shuffle(validGuilds)


    for i in range(len(validGuilds)):
        channels, idNames = utils.getChannelsRemote(token, validGuilds[i],  exePath, idNames)
        validChannels = []
        chErr = utils.loadIDFile(paths['channelUnavailable'])
        chDone = utils.getChannelsLocal(zipDir)
        for channel in channels:
            # Make this more legible
            if forceRedownload or channel not in chDone+chErr or (channel in chErr and forceRetryChannels):
                validChannels.append(channel)
        print("\nGuild {}/{} ({}): found {}/{} valid channels".format(i+1, len(validGuilds), idNames['g'][validGuilds[i]], len(validChannels), len(channels)))
        j = 0
        numChannelsToDownload = len(validChannels) 
        tried = []
        while len(validChannels) > 0:
            random.shuffle(validChannels)
            j += 1
            channel = validChannels[0]
            print('Channel {}/{}: #{} [{}]'.format(j, numChannelsToDownload, idNames['c'][channel], channel))
            tried.append(channel)
            result = downloadChannelJson(token, channel, downloadPath, exePath, idNames)
            validChannels = []
            chErr = utils.loadIDFile(paths['channelUnavailable'])
            chDone = utils.getChannelsLocal(zipDir)
            for channel in channels:
                if forceRedownload or channel not in chDone+chErr+tried or (channel in chErr and forceRetryChannels):
                    validChannels.append(channel)
        # Updating logs:
        utils.logAppend(paths['guild'], validGuilds[i])
        with codecs.open(nameLog, 'w', encoding='utf-8', errors='replace') as f:
            f.write(json.dumps(idNames, sort_keys=True, indent=4))
            

        # print("Found {}/{} valid channels".format(len(validChannels),len(channels)))
        # for channel in validChannels:
            
        
