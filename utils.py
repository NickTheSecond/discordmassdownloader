import os, codecs, platform, subprocess
from os.path import join
import ntpath, bz2

def getSizeHuman(num):
    for unit in ['B','KB','MB','GB', 'TB']:
        if abs(num) < 1024.0:
            return "%3.1f%s" % (num, unit)
        num /= 1024.0
    return "error"

def unzipChannel(channel, unzipDir, zipDir):
    files = os.listdir(zipDir)
    toUnzip = ''
    for filename in files:
        if channel in filename:
            if len(filename) > 4 and filename[-4:] == '.bz2':
                toUnzip = filename
                break
    if toUnzip == '':
        return
    unzipPath = join(unzipDir, toUnzip)[:-4]
    zipPath = join(zipDir, toUnzip)
    print('Unzipping {} -> '.format(getSizeHuman(os.path.getsize(zipPath))), end='')
    inFile = bz2.BZ2File(zipPath, 'rb')
    outFile = open(unzipPath, 'wb')
    outFile.write(inFile.read())
    outFile.close()
    inFile.close()
    print(getSizeHuman(os.path.getsize(unzipPath)))
    return zipPath, unzipPath

def ensureExists(path):
    if os.path.exists(path):
        return
    parentDir, shortPath = ntpath.split(path)
    if not os.path.isdir(parentDir):
        os.makedirs(parentDir)
    if "." in shortPath:
        with open(path, "w") as f:
            None
    else:
        os.mkdir(path)

def logAppend(filename, data):
    contents = loadIDFile(filename)
    if str(data) not in contents:
        with open(filename, 'a') as f:
            f.write(data+'\n')

def loadIDFile(filename):
    result = []
    with open(filename, 'r') as f:
        for line in f.readlines():
            # print([line])
            if line[-1] == "\n" and line not in result:
                result.append(line[:-1])
            else:
                result.append(line)
    with open(filename, 'w') as f:
        for line in result:
            f.write(line+'\n')
    return result

def parseOutput(output):
    if type(output) == type(b''):
        output = output.decode('utf-8', errors='replace')
    ids = []
    names = []
    output.replace('\r','')
    for line in output.split('\n'):
        if line == '':
            continue
        num = line[:line.find(' ')]
        name = line[line.find('|')+2:]
        ids.append(num)
        names.append(name)
    return ids, names

def getGuilds(token, exePath, idNames=None, verbosity=2):
    command = [exePath, 'guilds', '-t', token]
    if verbosity >= 3:
        print(' '.join(command))
    returnValue = subprocess.check_output(' '.join(command), shell=True).decode('utf-8', errors='replace').replace('\r','')
    guildList, guildNames = parseOutput(returnValue)
    if idNames is not None:
        for i in range(len(guildList)):
            idNames['g'][guildList[i]] = guildNames[i]
        return guildList, idNames
    return guildList

def getChannelsRemote(token, guild, exePath, idNames=None):
    command = ' '.join([exePath, 'channels', '-t', token, '-g', guild])
    # if platform.system() == 'Windows':
        # command = ' '.join([exePath[:exePath.find(':')]+': &&', exePath, 'channels', '-t', token, '-g', guild])
    returnValue = subprocess.check_output(command, shell=True).decode('utf-8', errors='replace').replace('\r','')
    channelList, channelNames = parseOutput(returnValue)
    if idNames is not None:
        for i in range(len(channelList)):
            idNames['c'][channelList[i]] = channelNames[i]
        return channelList, idNames
    return channelList

def getChannelsLocal(path):
    files = os.listdir(path)
    channels = []
    for filename in files:
        while filename.count('[') > 0:
            filename = filename[filename.find('[')+1:]
        channels.append(filename[:filename.find(']')])
    return channels