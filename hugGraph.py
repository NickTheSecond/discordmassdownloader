
import os, codecs


import networkx as nx           # For making graphs,manipulation etc
import matplotlib.pyplot as plt # For plotting the graphs
import numpy as np # Matrix manipulation
# import seaborn as sns # plotting

def findUsers(filelist):
    users = []
    for filename in filelist:
        with codecs.open(filename, 'r', encoding='UTF-8') as f:
            lastLine = ""
            for line in f:
                if lastLine == "\r\n" and "[" in line and "]" in line and "#" in line:
                    if "AM" in line or "PM" in line: # More checks, dependent on time format
                        username = line[line.find("]")+2:-2]
                        # print([username])
                        if username not in users:
                            users.append(username)
                lastLine = line
    print("Found {} unique users".format(len(users)))
    users.sort()
    return users

def findHugs(filelist):
    triggers = ["+hug"]
    hugs = []
    users = []
    lines = [] # Don't use this
    for filename in filelist:
        print("Searching for hugs in {}".format(filename))
        lastLine = ""
        with codecs.open(filename, 'r', encoding='UTF-8') as f:
            for line in f:
                lines.append(line)
                # Finding users
                if lastLine == "\r\n" and "[" in line and "]" in line and "#" in line:
                    if "AM" in line or "PM" in line: # More checks, dependent on time format
                        username = line[line.find("]")+2:-2]
                        # print([username])
                        if username not in users:
                            users.append(username)
                
                # Checking for a hug
                matches = False
                for trigger in triggers:
                    if line[:len(trigger)] == trigger:
                        # print(lastLine[:-2])
                        # print(line[:-2])
                        sender = lastLine[lastLine.find("]")+2:-2]
                        indexStart = len(trigger)+1
                        while line[indexStart] == " ": 
                            indexStart += 1
                        recipientList = line[indexStart+1:-2].split("@")
                        recipients = []
                        if recipientList == [""] or "@" not in line:
                            continue # Protects against hugging the void
                        if lastLine[0] != "[":
                            continue # Protects against initiating a hug on a second line (looking at you mees)
                        for name in recipientList: # In case of muliple hug recipients
                            if name[-1] == " ":
                                name = name[:-1]
                            name = name
                            recipients.append(name)
                        if len(recipients) > 2:
                            print("\tMultihug: {} recipients".format(len(recipients)))
                        for recipient in recipients: # Multiple hug targets are treated as individual hugs
                            hugs.append([sender, recipient])
                            None
                lastLine = line
        print("Total found: {} hugs".format(len(hugs)))
    print("Cleaning up hugs...")

    # Resolving @s from pings into usernames
    users.sort()
    hugsClean = []
    shortnames = [user[:-5] for user in users] # users but without the # identifier
    for i in range(len(hugs)):
        pingName = hugs[i][1]
        newName = ""
        for j in range(len(users)):
            if pingName[:len(shortnames[j])] == shortnames[j]:
                newName = shortnames[j]
                hugsClean.append([hugs[i][0], users[j]])
        if newName == "":
            print("Unmatched ping tag: {}".format([hugs[i][1]]))
            None
    return users, hugsClean

os.chdir(os.path.dirname(os.path.realpath(__file__)))

files = [os.path.join("downloadsTxt", file) for file in os.listdir("downloadsTxt")]

candy = []
for filename in files:
    if "Candy" in filename:
        candy.append(filename)

# Finding list of users
# users = findUsers(candy)

# for user in users:
#     if user.count("#") > 1:
#         print(user)

users, hugs = findHugs(candy)

# Removing self-hugs (not sure what to do with them)
removedSelfHugs = 0
for hug in hugs:
    if hug[0] == hug[1]:
        hugs.remove(hug)
        removedSelfHugs += 1
print("Removed {} self-hugs".format(removedSelfHugs))
removedSelfHugs = None

# Compiling statistics about hugs (not required)
senders = {}
recievers = {}
for hug in hugs:
    count = 1
    if hug[0] in senders:
        count = senders[hug[0]] + 1
    senders[hug[0]] = count
    count = 0
    if hug[1] in recievers:
        count = recievers[hug[1]] + 1
    recievers[hug[1]] = count
print("Total hugs: {}".format(len(hugs)))



# For measuring activity and pruning inactive users
userSent = [[0 for j in range(len(users))] for i in range(len(users))] # [sender][recipient]
userRecieved = [[0 for j in range(len(users))] for i in range(len(users))] # [recipient][sender] - I know this is a stupid way of doing it
userParticipation = [0 for i in range(len(users))]
activeUsers = []
for hug in hugs:
    userSent[users.index(hug[0])][users.index(hug[1])] += 1
    userParticipation[users.index(hug[1])] += 1
for i in range(len(users)):
    if userParticipation[i] > 0:
        activeUsers.append(users[i])

# Graph stuff
G = nx.Graph()
plt.figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')

edgeWeight = []
for user in activeUsers:
    G.add_node(user)
for i in range(len(users)):
    for j in range(len(users)):
        interactions = userSent[i][j] + userSent[j][i]
        if interactions > 0:
            # G.add_edge(users[i], users[j], weight=interactions)
            G.add_edge(users[i], users[j], weight=1/interactions)
            edgeWeight.append(interactions)

edgeWeight = [n/max(edgeWeight) for n in edgeWeight]

pos = nx.spring_layout(G) 
edges = [ i for i in G.edges(data=True)]
for i in range(len(edges)):
    nx.draw_networkx_edges(G, pos, edgelist=[edges[i]],
                       width=1, alpha=edgeWeight[i], edge_color='b')


# usersDict = {}
# for i in range(len(activeUsers)):
#     usersDict[i] = activeUsers[i]
nx.draw_networkx_nodes(G, pos, node_size=100)
# nx.draw_networkx_labels(G, pos, usersDict, font_size=16)
# nx.draw(G, with_labels=True)

# pos = nx.spring_layout(G)  # positions for all nodes

# # nodes
# nx.draw_networkx_nodes(G, pos, node_size=700)

# # edges
# nx.draw_networkx_edges(G, pos, edgelist=G.edges, width=6)
# # nx.draw_networkx_edges(G, pos, edgelist=esmall,
#                        width=6, alpha=0.5, edge_color='b', style='dashed')

# # labels
# nx.draw_networkx_labels(G, pos, font_size=20, font_family='sans-serif')

# plt.axis('off')

plt.show()


print("Done!")